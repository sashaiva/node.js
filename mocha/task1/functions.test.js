import { assert } from 'chai';
import { factorial } from './functions.js';

// Починаємо опис тестів
describe('Factorial Function Test Suite', () => {
    // Тест для 0!
    it('should return 1 for 0!', () => {
        const result = factorial(0);
        assert.equal(result, 1);
    });

    // Тест для 1!
    it('should return 1 for 1!', () => {
        const result = factorial(1);
        assert.equal(result, 1);
    });

    // Тест для 5!
    it('should return 120 for 5!', () => {
        const result = factorial(5);
        assert.equal(result, 120);
    });

    // Тест для 10!
    it('should return 3628800 for 10!', () => {
        const result = factorial(10);
        assert.equal(result, 3628800);
    });
});
