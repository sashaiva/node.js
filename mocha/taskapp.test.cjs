const assert = require('assert');
const request = require('supertest');
const app = require('../lab3/src/app');

describe('User Registration API Tests', function() {

    it('should return 401 for registration with invalid data', async function() {
        const userData = {
            firstName: "John",
            lastName: "Doe",
            email: "invalid_email",
            password: "weak",
            age: -10
        };

        const response = await request(app)
            .post('/users')
            .send(userData);

        assert.strictEqual(response.status, 401);
        assert.strictEqual(response.body.error, 'User validation failed: email: Invalid email address, password: Path `password` (`weak`) is shorter than the minimum allowed length (7)., age: Age must be a positive number');
    });

    it('should successfully register user with valid data', async function() {
        const userData = {
            firstName: "user6",
            lastName: "user6",
            email: "user6@gmail.com",
            password: "123456786",
            age: 16
        };

        const response = await request(app)
            .post('/users')
            .send(userData);

        assert.strictEqual(response.status, 201);
        assert.ok(response.body._id);
    });

    it('should successfully register another user with valid data', async function() {
        const userData = {
            firstName: "user6",
            lastName: "user2",
            email: "user7@gmail.com",
            password: "123456786",
            age: 16
        };

        const response = await request(app)
            .post('/users')
            .send(userData);

        assert.strictEqual(response.status, 201);
        assert.ok(response.body._id);
    });
    it('should login as User1 with valid credentials', async () => {
        // Вхід під User1 з вірними даними
        const userData = {
            email: "user1@gmail.com",
            password: "123456786"
        };
        const response = await request(app)
            .post('/users/login')
            .send(userData);

        assert.strictEqual(response.status, 200);
        assert.strictEqual(response.body.message, 'success');
    });
    it('should add Task1 with status 200 and get task object with an identifier', async () => {
        // Додавання задачі Task1
        const taskData = {
            title: "Task1"
        };
        const response = await request(app)
            .post('/taskss')
            .send(taskData);

        assert.strictEqual(response.status, 200);
        assert.ok(response.body._id); // Перевірка, що ідентифікатор існує у відповіді
    });

    it('should add Task2 with status 200 and get task object with an identifier', async () => {
        // Додавання задачі Task2
        const taskData = {
            title: "Task2"
        };
        const response = await request(app)
            .post('/taskss')
            .send(taskData);

        assert.strictEqual(response.status, 200);
        assert.ok(response.body._id); // Перевірка, що ідентифікатор існує у відповіді
    });

    it('should retrieve tasks for User1 with status 200 and length 2', async () => {
        // Отримання задач користувача User1
        const response = await request(app)
            .get('/tasks');

        assert.strictEqual(response.status, 200);
        assert.strictEqual(response.body.length, 2); // Перевірка, що отримано дві задачі
    });

    it('should retrieve Task1 by its identifier with status 200 and object with title and completed properties', async () => {
        // Отримуємо задачу Task1 по ідентифікатору
        const tasksResponse = await request(app)
            .get('/tasks');

        const task1Id = tasksResponse.body[0]._id; // Припускаємо, що Task1 - перша задача у відповіді
        const response = await request(app)
            .get(`/tasks/${task1Id}`);

        assert.strictEqual(response.status, 200);
        assert.strictEqual(response.body.title, 'Task1'); // Перевірка назви задачі
        assert.strictEqual(response.body.hasOwnProperty('completed'), true); // Перевірка властивості completed
    });

    it('should logout successfully with a message "logout success"', async () => {
        // Вихід
        const response = await request(app)
            .post('/users/logout')
            .set('Authorization', `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NjJlOTA1YmRkODNiYzM4OTk2YmUzNjUiLCJpYXQiOjE3MTQzMjk3NzN9.Xxpa6ZqIpUX3xulQGxA1ENVl977BuDn73MUq1Ks1eqA`); // Передача токену для автентифікації

        assert.strictEqual(response.status, 200);
        assert.strictEqual(response.body.message, 'logout success'); // Перевірка повідомлення про вихід
    });
    it('should login successfully for User2 with status 200 and received message "success"', async () => {
        // Вхід під User2 з вірними даними
        const loginData = {
            email: "user2@gmail.com",
            password: "123456786"
        };

        const response = await request(app)
            .post('/users/login')
            .send(loginData);

        assert.strictEqual(response.status, 200);
        assert.strictEqual(response.body.message, 'success');
    });

    it('should add Task3 with status 200 and get task object with an identifier', async () => {
        // Додавання задачі Task3
        const taskData = {
            title: "Task3"
        };
        const response = await request(app)
            .post('/taskss')
            .send(taskData);

        assert.strictEqual(response.status, 200);
        assert.ok(response.body._id); // Перевірка, що ідентифікатор існує у відповіді
    });
    it('should get tasks for User2 with status 200 and length 1', async () => {
        // Отримання задач користувача User2
        const response = await request(app)
            .get('/tasks')
            .set('Authorization', `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NjJlOTA1YmRkODNiYzM4OTk2YmUzNjUiLCJpYXQiOjE3MTQzMjk3NzN9.Xxpa6ZqIpUX3xulQGxA1ENVl977BuDn73MUq1Ks1eqA`);

        assert.strictEqual(response.status, 200);
        assert.strictEqual(response.body.length, 1); // Перевірка, що отримано лише одну задачу
    });

    it('should get Task1 by its identifier with status 404 and message "Not Found"', async () => {
        // Отримуємо задачу Task1 по її ідентифікатору
        const taskId = 'some_nonexistent_task_id'; // Припустимо, що такого ідентифікатора немає
        const response = await request(app)
            .get(`/tasks/${taskId}`)
            .set('Authorization', `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NjJlOTA1YmRkODNiYzM4OTk2YmUzNjUiLCJpYXQiOjE3MTQzMjk3NzN9.Xxpa6ZqIpUX3xulQGxA1ENVl977BuDn73MUq1Ks1eqA`);

        assert.strictEqual(response.status, 404);
        assert.strictEqual(response.body.message, 'Not Found');
    });

    it('should successfully logout with status 200 and message "logout success"', async () => {
        // Вихід
        const response = await request(app)
            .post('/users/logout')
            .set('Authorization', `Bearer none`);

        assert.strictEqual(response.status, 200);
        assert.strictEqual(response.body.message, 'logout success');
    });

    it('should get Task1 by its identifier with status 403 and message "Forbidden Access"', async () => {
        // Отримуємо задачу Task1 по її ідентифікатору
        const response = await request(app)
            .get(`/tasks/662e91c8dd83bc38996be36e`)
            .set('Authorization', `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NjJlOTA1YmRkODNiYzM4OTk2YmUzNjUiLCJpYXQiOjE3MTQzMjk3NzN9.Xxpa6ZqIpUX3xulQGxA1ENVl977BuDn73MUq1Ks1eqA`);

        assert.strictEqual(response.status, 403);
        assert.strictEqual(response.body.message, 'Forbidden Access');
    });

});
