const yargs = require('yargs');
const fs = require('fs');
const User = require('./user');

const loadUser = () => {
    try {
        const userData = fs.readFileSync('user.js.json', 'utf-8');
        return new User(...JSON.parse(userData));
    } catch (error) {
        return new User('Olexandr', 'Ivaschenko');
    }
};

const saveUser = (user) => {
    const userData = JSON.stringify([user.firstName, user.lastName, user.languages], null, 2);
    fs.writeFileSync('user.js.json', userData, 'utf-8');
};

const user = loadUser();

yargs
    .command({
        command: 'add',
        describe: 'Додати мову користувача',
        builder: {
            title: {
                describe: 'Назва мови',
                demandOption: true,
                type: 'string',
            },
            level: {
                describe: 'Рівень володіння',
                demandOption: true,
                type: 'string',
            },
        },
        handler: (argv) => {
            user.addLanguage({ title: argv.title, level: argv.level });
            saveUser(user);
        },
    })
    .command({
        command: 'view',
        describe: 'Переглянути всі мови користувача',
        handler: () => {
            user.viewLanguages();
        },
    })
    .command({
        command: 'remove',
        describe: 'Видалити мову користувача',
        builder: {
            title: {
                describe: 'Назва мови для видалення',
                demandOption: true,
                type: 'string',
            },
        },
        handler: (argv) => {
            user.removeLanguage(argv.title);
            saveUser(user);
        },
    })
    .argv;