const fs = require('fs');

const message = 'Hello, World!';
const filename = 'output.txt';
//читаємо
let currentContent = '';
try {
    currentContent = fs.readFileSync(filename, 'utf-8');
} catch (error) {

}
//записуємо
const newContent = `${currentContent}\n${message}`;
fs.writeFileSync(filename, newContent, 'utf-8');

console.log(`"${message}" було додано до файлу ${filename}`);