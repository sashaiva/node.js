const _ = require('lodash');

// Тестування методів lodash

//map
//перетворення кожного елемента масиву arr у його квадрат
const arr = [1, 2, 3, 4, 5];
const squaredArr = _.map(arr, (num) => num * num);
console.log('1. Метод map:', squaredArr);

//filter
//відбір лише парних чисел з масиву arr
const evenNumbers = _.filter(arr, (num) => num % 2 === 0);
console.log('2. Метод filter:', evenNumbers);

//find
//find повертає перший елемент масиву arr, який задовольняє умову
const firstEvenNumber = _.find(arr, (num) => num % 2 === 0);
console.log('3. Метод find:', firstEvenNumber);

//sortBy
//sortBy сортує елементи масиву unsortedArr в зростаючому порядку
const unsortedArr = [5, 3, 8, 1, 4];
const sortedArr = _.sortBy(unsortedArr);
console.log('4. Метод sortBy:', sortedArr);

//includes
//includes повертає true, якщо масив arr містить число 3, інакше повертає false
const includesThree = _.includes(arr, 3);
console.log('5. Метод includes:', includesThree);