class User {
    constructor(firstName, lastName, languages = []) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.languages = languages;
    }

    addLanguage(language) {
        // Валідація: Перевірка, чи мова не дублюється
        const existingLanguage = this.languages.find(lang => lang.title === language.title);
        if (!existingLanguage) {
            this.languages.push(language);
            console.log(`Мова ${language.title} успішно додана.`);
        } else {
            console.log(`Мова ${language.title} вже існує.`);
        }
    }

    viewLanguages() {
        console.log(`Мови користувача ${this.firstName} ${this.lastName}:`);
        this.languages.forEach(lang => {
            console.log(`- ${lang.title} (${lang.level})`);
        });
    }

    removeLanguage(languageTitle) {
        // Валідація: Перевірка, чи мова існує перед видаленням
        const index = this.languages.findIndex(lang => lang.title === languageTitle);
        if (index !== -1) {
            const removedLanguage = this.languages.splice(index, 1);
            console.log(`Мова ${removedLanguage[0].title} успішно видалена.`);
        } else {
            console.log(`Мова ${languageTitle} не знайдена.`);
        }
    }
}

module.exports = User;