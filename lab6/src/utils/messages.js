// messages.js
const generateMessage = (username, text, avatar) => {
    return {
        username,
        text,
        avatar,
        createdAt: new Date().getTime()
    }
}

const generateLocationMessage = (username, url, avatar) => {
    return {
        username,
        url,
        avatar,
        createdAt: new Date().getTime()
    }
}

module.exports = {
    generateMessage,
    generateLocationMessage
}
