const express = require('express')
const http = require('http')
const socketio = require('socket.io')

const {generateMessage, generateLocationMessage} = require("./utils/messages")
const {addUser, removeUser, getUser, getUsersInRoom} = require('./utils/users')

const app = express()
const server = http.createServer(app)
const io = socketio(server)

app.use(express.static(__dirname + '/../public'));

// app.js
io.on('connection', (socket) => {

    socket.on('join', (options, callback) => {
        const { error, user } = addUser({ id: socket.id, ...options })

        if (error) {
            return callback(error)
        }

        socket.join(user.room)

        socket.emit('message', generateMessage('Admin', `Welcome, ${user.username}!`, user.avatar))
        socket.broadcast.to(user.room).emit('message', generateMessage('Admin', `${user.username} has joined!`, user.avatar))
        io.to(user.room).emit('roomData', {
            room: user.room,
            users: getUsersInRoom(user.room)
        })

        callback()
    })

    socket.on('sendPrivateMessage', ({ recipient, text }, callback) => {
        const sender = getUser(socket.id);
        const recipientUser = getUsersInRoom(sender.room).find(user => user.username === recipient);

        if (!recipientUser) {
            return callback('Recipient not found!');
        }

        io.to(recipientUser.id).emit('message', generateMessage(sender.username, text, sender.avatar));
        callback();
    });

    socket.on('sendMessage', (message, callback) => {
        const user = getUser(socket.id)

        io.to(user.room).emit('message', generateMessage(user.username, message, user.avatar))
        callback()
    })

    socket.on('userIsTyping', () => {
        const user = getUser(socket.id);
        if (user) {
            socket.to(user.room).emit('userIsTyping', { username: user.username, avatar: user.avatar });
            setTimeout(() => {
                socket.to(user.room).emit('userStoppedTyping');
            }, 2000);
        }
    });

    socket.on('disconnect', () => {
        const user = removeUser(socket.id)

        if (user) {
            io.to(user.room).emit('message', generateMessage('Admin', `${user.username} has left!`, user.avatar))
            io.to(user.room).emit('roomData', {
                room: user.room,
                users: getUsersInRoom(user.room)
            })
        }
    })
});


server.listen(3000, function () {
    console.log('listening on *:3000');
});