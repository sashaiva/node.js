const express = require("express");
const hbs = require("hbs");
const fetch = require("node-fetch")
let app = express();
hbs.registerPartials(__dirname + '/views/partials');

app.set('view engine', 'hbs');
app.get('/', (req, res) => {
    res.render('main.hbs');
});

// Семантичний URL
app.get('/weather/:city', async (req, res) => {

    res.render('weather.hbs', { city: req.params.city });
});

app.get('/weather',async (req, res) => {
    const city = req.query.city;
    let key = 'a51af90b16460af09f1361d43559f0b8';
    let url = `https://api.openweathermap.org/data/2.5/weather?lat={lat}&q=${city}&appid=${key}&units=metric`;
    let response = await fetch(url);
    let weather = await response.json();
    if (city) {
        res.render('weather.hbs', {city:city,weather: weather});
    } else {

    }

});



app.get('/login',(req,res)=>{
    res.send("Login");
});

app.listen(3000, () => {
    console.log("Example app listening on port 3000");
});
