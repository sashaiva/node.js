const jwt = require("jsonwebtoken");
const User = require("../../models/user");

const auth = async (req, res, next) => {
    try {
        const authHeader = req.header('Authorization');
        if (!authHeader || !authHeader.startsWith('Bearer ')) {
            throw new Error('Missing or invalid Authorization header');
        }

        const token = authHeader.split(" ")[1];
        const decoded = jwt.verify(token, 'sadasdasdasdasd');

        const user = await User.findById(decoded._id);
        if (!user) {
            throw new Error('Invalid token');
        }

        req.user = user;
        req.token = token;
        next();
    } catch (e) {
        console.error(e);
        res.status(401).send({ error: "Authentication failed" });
    }
};

module.exports = auth;