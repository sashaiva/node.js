const express = require("express");
const User = require("../../models/user");
const router = new express.Router();
const auth = require('.././middleware/auth');


router.get("/users/:id", async (req, res) => {
    const userId = req.params.id;
    try {
        const user = await User.findById(userId);
        if (!user) {
            return res.status(404).json({ message: "User not found" });
        }
        res.status(200).json(user);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});

router.post("/users", async (req, res) => {
    const userData = req.body;
    try {
        const newUser = await User.create(userData);
        res.status(201).json(newUser);
    } catch (error) {
        res.status(401).json({ error: error.message });
    }
});

router.get("/users", async (req, res) => {
    try {
        const users = await User.find({});
        res.status(200).json(users);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});
router.delete("/users/:id", async (req, res) => {
    const userId = req.params.id;
    try {
        const user = await User.findByIdAndDelete(userId);
        if (!user) {
            return res.status(404).json({ message: "User not found" });
        }
        res.status(200).json({ message: "User deleted successfully" });
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
});
router.patch("/users/:id", async (req, res) => {
    try {
        const user = await User.findOne({ _id: req.params.id });
        if (!user) {
            return res.status(404).json({ error: "User not found" });
        }
        const fields = ["firstName", "lastName", "email", "password", "age"];
        fields.forEach((field) => {
            if (req.body[field]) {
                user[field] = req.body[field];
            }
        });
        await user.save();
        res.json(user);
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
});

router.post("/users/logout", auth, async (req, res) => {
    try {
        console.log("Logout route called");
        // Фільтрація токенів користувача, залишаючи лише ті, які не дорівнюють поточному токену
        req.user.tokens = req.user.tokens.filter(tokenObj => tokenObj.token !== req.token);

        console.log("Tokens after filter:", req.user.tokens); // Додайте цей рядок для перевірки, які токени залишилися після фільтрації

        // Збереження змін у базі даних
        await req.user.save();

        res.status(200).json({ message: "Logged out successfully" });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});


router.post("/users/login", async (req, res) => {
    try {
        const user = await User.findOneByCredentials(req.body.email, req.body.password);
        if (!user) {
            throw new Error('Incorrect credentials');
        }
        const token = await user.generateAuthToken();
        res.send({ user, token });
    } catch (e) {
        res.status(400).send({ error: e.message });
    }
});

router.get("/user/me", auth, async (req, res) => {
    try {
        res.status(200).json(req.user); // req.user содержит данные текущего пользователя
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});


module.exports = router;
