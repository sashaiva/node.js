const express = require("express");
const Task = require("../../models/task");
const router = new express.Router();
const auth = require('.././middleware/auth');

// Отримання задачі по її id для користувача, якому належить
router.get("/tasks/:id", auth, async (req, res) => {
    const taskId = req.params.id;
    try {
        const task = await Task.findOne({ _id: taskId, owner: req.user._id });
        if (!task) {
            return res.status(404).json({ message: "Task not found" });
        }
        res.status(200).json(task);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});

// Модифікація методу отримання списку задач лише для поточного користувача
router.get("/tasks", auth, async (req, res) => {
    try {
        const tasks = await Task.find({ owner: req.user._id });
        res.status(200).json(tasks);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});
// Метод редагування задачі, яка належить поточному користувачеві
router.patch("/tasks/:id", auth, async (req, res) => {
    const updates = Object.keys(req.body);
    const allowedUpdates = ['title', 'description', 'completed'];
    const isValidOperation = updates.every(update => allowedUpdates.includes(update));

    if (!isValidOperation) {
        return res.status(400).send({ error: 'Invalid updates!' });
    }

    try {
        const task = await Task.findOne({ _id: req.params.id, owner: req.user._id });
        if (!task) {
            return res.status(404).send({ error: 'Task not found' });
        }

        updates.forEach(update => task[update] = req.body[update]);
        await task.save();

        res.status(200).json(task);
    } catch (error) {
        res.status(400).send(error);
    }
});

// Метод видалення задачі, яка належить поточному користувачеві
router.delete("/tasks/:id", auth, async (req, res) => {
    try {
        const task = await Task.findOneAndDelete({ _id: req.params.id, owner: req.user._id });
        if (!task) {
            return res.status(404).json({ message: "Task not found" });
        }
        res.status(200).json({ message: "Task deleted successfully" });
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
});

router.post("/taskss",auth,async (req,res)=>{
const task = new Task({
    ...req.body,
    owner: req.user.id
});
try{
    await task.save();
    res.status(201).send(task);
}catch (e){
    res.status(500).send(e);
}
});

module.exports = router;