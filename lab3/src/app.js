const express = require("express");
const mongoose = require("mongoose").default;
const User = require("../models/user");
const Task = require("../models/task");
const userRouter = require('./routers/user');
const taskRouter = require('./routers/task');

require("dotenv").config({ path: '../.env' });

const app = express();
const port = process.env.PORT || 3000;

const url = process.env.MONGO_URL;

async function connectToDatabase() {
    try {
        await mongoose.connect(url, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        console.log("Connected to MongoDB");
    } catch (error) {
        console.error("Error connecting to MongoDB:", error.message);
    }
}

connectToDatabase();


app.use(express.json());

app.use(userRouter);
app.use(taskRouter);

app.listen(port, () => {
    console.log(`Server is listening on ${port} port`);
});
module.exports = app;
