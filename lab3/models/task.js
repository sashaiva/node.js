const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        trim: true // Обрізка крайніх пробілів
    },
    description: {
        type: String,
        required: true,
        trim: true // Обрізка крайніх пробілів
    },
    completed: {
        type: Boolean,
        default: false // Значення за замовчуванням
    },
    owner:{
        type: mongoose.Schema.Types.ObjectId,
        ref:"User",
        required: true
    }
});

const Task = mongoose.model("Task", taskSchema);

module.exports = Task;
